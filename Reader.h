//By: Terrence Reardon
#pragma once
#ifndef READER_H
#define READER_H

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <utility> // std::pair
#include <stdexcept> // std::runtime_error
#include <sstream> // std::stringstream
#include <regex>

#include <boost/tokenizer.hpp>// use boost library to create the tokenizer
//#include "MyForm.h"
const int maxItems = 15;
class Reader {
private:
	//Input
	//int* arr = new int();
	std::ifstream inFile;
	std::ofstream outFile;
public:

	/*Main functions*/
	//Reads files
	std::string NoloFinder(std::string bookstore, std::string roomreport, std::string outputNolo, int NoLoNum);
	std::string No2nd(std::string bookstore, std::string outputNolo, int NoLoNum);

	
	
	//Catches overflow of rows
	std::string regEndCatcher(std::string crust);

	//Checks the keys such as REQ
	bool keyCatcher(std::string key);

	//convert into a double
	double priceConverter(std::string toConvert);

};
#endif
struct bookStruct {
	std::string CRN;//course number																						registrar csv
	std::string Subject;//subject of the class															bookstore csv
	std::string Number;//class number																	bookstore csv
	std::string Section;//section of the specific class													bookstore csv
	std::string CourseName;//name of the course																			registrar csv
	double Cost = 0;//name of the course																bookstore csv
	std::string LastName;//professor last name															bookstore csv
	std::string FirstName;//professor first name														bookstore csv
	std::string Email;//professor email																					registrar csv


	std::string key;//this is the REQ and similar tags													bookstore csv
	std::string NewCost;//first cost read in from bookstore csv
	double convCost = 0;//converted cost from the string NewCost
	//find with totalCostCreater
	double totalCost = 0;//**add conv costs together

	int courseCounter = 0;//tracks whether a new course or not
};
