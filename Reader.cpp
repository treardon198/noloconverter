//By: Terrence Reardon
#include "Reader.h"






//read the bookstore file
std::string Reader::NoloFinder(std::string bookstore, std::string roomreport, std::string outputNolo, int NoLoNum) {
    inFile.open(bookstore);
    outFile.open(outputNolo);
    bookStruct bs[900];

    //if everything is working properly
    if (inFile) {
        //this will be the header for the output csv
        outFile << "CRN, SUBJECT, NUMBER, SECTION, COURSE NAME, "
            "COST, NOLO, INSTRUCTOR LAST NAME, INSTRUCTOR FIRST NAME, INSTRUCTOR EMAIL\n";

        std::string line;
        int counter = 0;
        //use boost libraries to tokenize items
        using namespace boost;

        int courseCounter = 0;//tracks whether a new course or not
        int courseNameCounter = 0;//counter for writing the file
        int scs = 0;//for splitting into course, number, and section
        bool lastorfirst = false;
        //read in the lines
        while (std::getline(inFile, line)) {
            //std::cout << "line: " << line << "\n";// for testing, outputs read line

            //if line not empty
            if (line.length() > 0) {
                // Ref: https://www.boost.org/doc/libs/1_76_0/libs/tokenizer/doc/escaped_list_separator.htm
                boost::tokenizer < boost::escaped_list_separator<char> >tok(line);
                int tokenNumber = 0;//tracks commas skipped
                bool costToggle = false;//toggles for first or second cost(new/used)
                bool notKey = false;

                //reads part of line seperated by commas
                for (boost::tokenizer < boost::escaped_list_separator<char> > ::iterator beg = tok.begin(); beg != tok.end(); ++beg) {
                    //all the following are for testing lines
                    std::string copy = *beg;
                    std::size_t found = copy.find("$");//find dollar sign
                    std::size_t nfound = copy.find("N");//capital N is used in each class number, makes it easy to find
                    std::size_t mfound = copy.find(",");//comma compare
                    std::size_t dfound = copy.find(".");//decimal compare
                    std::size_t cfound = copy.find(":");//colon compare



                    //if the snippet isnt empty
                    if ((*beg).length() > 0) {
                        //std::cout << tokenNumber << ": " << *beg << "\n";//for testing, check the token input

                        //this finds the start of the line and checks to find if it it a full course
                        if (nfound != std::string::npos && tokenNumber == 0 && cfound == std::string::npos) {//SUBJECT, NUMBER, SECTION***
                            //for testing

                            //increase counter for coursename to create a new struct
                            courseCounter++;

                            /*
                            The following code works as such:
                                Reads in the line from the file
                                Splits it based on space count
                                Since all the courses start with a space before them, it skips that space and sets the
                                Subject, Number, and Section based on their order
                            */
                            std::stringstream scshold;//this is an internal string to work the input
                            scshold << *beg;//puts the string read from the file into the stringstream
                            std::string line2;
                            while (std::getline(scshold, line2, ' ')) {//splits based on spaces
                                if (scs == 0) {//accounting for the first space
                                    scs++;
                                }
                                else if (scs == 1) {//set Subject
                                    bs[courseCounter].Subject = line2;
                                    scs++;
                                }
                                else if (scs == 2) {//set Number
                                    bs[courseCounter].Number = line2;
                                    scs++;
                                }
                                else if (scs == 3) {//set Section
                                    bs[courseCounter].Section = line2;
                                    scs++;
                                }
                                else {//adds to Section if there's more input
                                    bs[courseCounter].Section = bs[courseCounter].Section.append(" ");
                                    bs[courseCounter].Section = bs[courseCounter].Section.append(line2);
                                };
                            };
                            scs = 0;//reset the counter for the next 
                        }

                        //if it is false, it is the prior read to the upcoming names read
                        if (keyCatcher(copy) == false && tokenNumber == 0 && nfound == std::string::npos) {//if it is false, it is the prior read to the upcoming names read
                            notKey = true;
                        }

                        //reads professor names
                        if (mfound != std::string::npos && tokenNumber == 2 && notKey == true) {//PROFESSOR NAMES***
                            //for testing
                            //outFile << ", , , , , , , " << *beg << std::endl;
                            std::stringstream ss;
                            ss << *beg;
                            std::string line3;
                            while (std::getline(ss, line3, ',')) {

                                if (lastorfirst == false) {
                                    bs[courseCounter].LastName = line3;
                                    lastorfirst = true;
                                }
                                else {
                                    bs[courseCounter].FirstName = line3;
                                    lastorfirst = false;
                                };

                            };
                            notKey = false;

                        }

                        //check if REQ, REC, SUG, CHC, OER
                        if (keyCatcher(copy) == true) {//if it is
                            bs[courseCounter].key = *beg;
                        }

                        //cost finder
                        if (found != std::string::npos && costToggle == false) {//COSTS***
                            //outFile << ", , , , ,COST1" << *beg << std::endl;
                            bs[courseCounter].convCost = priceConverter(*beg);
                            bs[courseCounter].Cost = bs[courseCounter].convCost + bs[courseCounter].Cost;
                            costToggle = true;
                        }
                        else if (found != std::string::npos && costToggle == true) {
                            costToggle = false;
                        };
                        tokenNumber++;//this will increment the tracking int
                    };
                };
            };

        };

    };
    inFile.close();

    //Use this to map the position of the Nolo Courses from bookstore
    std::map<int, bookStruct>NOLOs;

    int courseNameCounter = 0;//to help set the data onto the map
    int mapCounter = 0;//used for for loops
    int Noloslength = 0;//save max length of NoLos map
    //Map the Nolos
    for (int a = 1; a < 900; a++) {
        if (bs[courseNameCounter].Cost < NoLoNum && keyCatcher(bs[courseNameCounter].key) == true) {//If NoLo
            NOLOs[mapCounter] = bs[courseNameCounter];//sets current position to map

            //good for testing errors
            if (!NOLOs[mapCounter].Subject.empty() && roomreport == "error.csv") {
                outFile << NOLOs[mapCounter].CRN << ","
                    << NOLOs[mapCounter].Subject << ","
                    << NOLOs[mapCounter].Number << ","
                    << NOLOs[mapCounter].Section << ","
                    << NOLOs[mapCounter].CourseName << ","
                    << NOLOs[mapCounter].Cost << ","
                    << "NOLO" << ","//NOLO
                    << NOLOs[mapCounter].LastName << ","
                    << NOLOs[mapCounter].FirstName << ","
                    << NOLOs[mapCounter].Email << std::endl;
            };
            /**/

            mapCounter++;//increment map
            Noloslength++;
        };
        courseNameCounter++;
    };

    inFile.open(roomreport);
    bookStruct rr[900];

    //if file not found
    if (roomreport != "error.csv") {
        std::cout << "File found" << std::endl << std::endl;
        std::string line;

        //tracks whether a new course or not
        int courseCounter = 0;
        //counter to keep track of place in the file
        int counter = 0;
        //counter to skip the first line of the file
        int skipfirst = 0;
        //While reading from file
        while (inFile) {

            //grab strings from input file, use commas to traverse
            std::getline(inFile, line, ',');

            //for reference, roomreport file has 31 columns but isnt 1:1 in terms off number positions
            //CRN(COURSE NUMBER)
            if (counter == 4 && skipfirst > 0) {
                rr[courseCounter].CRN = line;
                //std::cout << line << "\t";
            };
            //SUBJ(SUBJECT)
            if (counter == 5 && skipfirst > 0) {
                rr[courseCounter].Subject = line;
                //std::cout << line << "\t";
            };
            //CRSE(COURSE NUMBER)
            if (counter == 6 && skipfirst > 0) {
                rr[courseCounter].Number = line;
                //std::cout << line << "\t";
            };
            //SECT(SECTION)
            if (counter == 7 && skipfirst > 0) {
                rr[courseCounter].Section = line;
                //std::cout << line << "\t";
            };
            //COURSE NAME(COURSE NAME)
            if (counter == 8 && skipfirst > 0) {
                rr[courseCounter].CourseName = line;
                //std::cout << line << "\t";
            };
            //LNAME(LAST NAME)
            if (counter == 28 && skipfirst > 0) {
                rr[courseCounter].LastName = line;
                //std::cout << line << "\t";
            };
            //FNAME(FIRST NAME)
            if (counter == 29 && skipfirst > 0) {
                rr[courseCounter].FirstName = line;
                //std::cout << line << "\t";
            };
            //EMAIL...and BLDG(BUILDING) due to comma errors
            if (counter == 30 && skipfirst > 0) {
                //call endcatcher to trim overflow/merge of last cell and first
                rr[courseCounter].Email = regEndCatcher(line);
                //std::cout << regEndCatcher(line) << std::endl;
                counter = 0;
                courseCounter++;
            };

            //skips first line of file
            if (counter == 30 && skipfirst == 0) {
                counter = 0;
                skipfirst++;
            };
            //increment

            counter++;
        };
        //test
        //system("pause");
    };
    inFile.close();
    

    //reset the following two lines for the final comparison
    courseNameCounter = 0;
    mapCounter = 1;
    //compare NoLos
    for (int b = 1; b < 900; b++) {
        //If roomreport matching with NOLOS
        if (NOLOs[mapCounter].Subject == rr[b].Subject && NOLOs[mapCounter].Number == rr[b].Number && !rr[b].Subject.empty()) {

            //if next line would be a clone of this one do not output it
            if (rr[b].CRN != rr[b + 1].CRN) {
                //output the actual data! most is using the roomreport for section data
                outFile << rr[b].CRN << ","
                    << NOLOs[mapCounter].Subject << ","
                    << NOLOs[mapCounter].Number << ","
                    << rr[b].Section << ","
                    << rr[b].CourseName << ","
                    << NOLOs[mapCounter].Cost << ","
                    << "NOLO" << ","//NOLO
                    << rr[b].LastName << ","
                    << rr[b].FirstName << ","
                    << rr[b].Email;
            };
            //This will catch any extra sections
            if (NOLOs[mapCounter].Number != rr[b + 1].Number) {
                mapCounter++;
            }
        }
        //this will catch if a certain item is not found, move to the next one
        else if (b == 899 && mapCounter < Noloslength) {
            mapCounter++;
            b = 1;
        };
    };
    outFile.close();
    return bookstore;
};



//alternative to full set
std::string Reader::No2nd(std::string bookstore, std::string outputNolo, int NoLoNum) {
    inFile.open(bookstore);
    outFile.open(outputNolo);
    bookStruct bs[900];

    //if everything is working properly
    if (inFile) {
        //this will be the header for the output csv
        outFile << "CRN, SUBJECT, NUMBER, SECTION, COURSE NAME, "
            "COST, NOLO, INSTRUCTOR LAST NAME, INSTRUCTOR FIRST NAME, INSTRUCTOR EMAIL\n";

        std::string line;
        int counter = 0;
        //use boost libraries to tokenize items
        using namespace boost;

        int courseCounter = 0;//tracks whether a new course or not
        int courseNameCounter = 0;//counter for writing the file
        int scs = 0;//for splitting into course, number, and section
        bool lastorfirst = false;
        //read in the lines
        while (std::getline(inFile, line)) {
            //std::cout << "line: " << line << "\n";// for testing, outputs read line

            //if line not empty
            if (line.length() > 0) {
                // Ref: https://www.boost.org/doc/libs/1_76_0/libs/tokenizer/doc/escaped_list_separator.htm
                boost::tokenizer < boost::escaped_list_separator<char> >tok(line);
                int tokenNumber = 0;//tracks commas skipped
                bool costToggle = false;//toggles for first or second cost(new/used)
                bool notKey = false;

                //reads part of line seperated by commas
                for (boost::tokenizer < boost::escaped_list_separator<char> > ::iterator beg = tok.begin(); beg != tok.end(); ++beg) {
                    //all the following are for testing lines
                    std::string copy = *beg;
                    std::size_t found = copy.find("$");//find dollar sign
                    std::size_t nfound = copy.find("N");//capital N is used in each class number, makes it easy to find
                    std::size_t mfound = copy.find(",");//comma compare
                    std::size_t dfound = copy.find(".");//decimal compare
                    std::size_t cfound = copy.find(":");//colon compare



                    //if the snippet isnt empty
                    if ((*beg).length() > 0) {
                        //std::cout << tokenNumber << ": " << *beg << "\n";//for testing, check the token input

                        //this finds the start of the line and checks to find if it it a full course
                        if (nfound != std::string::npos && tokenNumber == 0 && cfound == std::string::npos) {//SUBJECT, NUMBER, SECTION***
                            //for testing

                            //increase counter for coursename to create a new struct
                            courseCounter++;

                            /*
                            The following code works as such:
                                Reads in the line from the file
                                Splits it based on space count
                                Since all the courses start with a space before them, it skips that space and sets the
                                Subject, Number, and Section based on their order
                            */
                            std::stringstream scshold;//this is an internal string to work the input
                            scshold << *beg;//puts the string read from the file into the stringstream
                            std::string line2;
                            while (std::getline(scshold, line2, ' ')) {//splits based on spaces
                                if (scs == 0) {//accounting for the first space
                                    scs++;
                                }
                                else if (scs == 1) {//set Subject
                                    bs[courseCounter].Subject = line2;
                                    scs++;
                                }
                                else if (scs == 2) {//set Number
                                    bs[courseCounter].Number = line2;
                                    scs++;
                                }
                                else if (scs == 3) {//set Section
                                    bs[courseCounter].Section = line2;
                                    scs++;
                                }
                                else {//adds to Section if there's more input
                                    bs[courseCounter].Section = bs[courseCounter].Section.append(" ");
                                    bs[courseCounter].Section = bs[courseCounter].Section.append(line2);
                                };
                            };
                            scs = 0;//reset the counter for the next 
                        }

                        //if it is false, it is the prior read to the upcoming names read
                        if (keyCatcher(copy) == false && tokenNumber == 0 && nfound == std::string::npos) {//if it is false, it is the prior read to the upcoming names read
                            notKey = true;
                        }

                        //reads professor names
                        if (mfound != std::string::npos && tokenNumber == 2 && notKey == true) {//PROFESSOR NAMES***
                            //for testing
                            //outFile << ", , , , , , , " << *beg << std::endl;
                            std::stringstream ss;
                            ss << *beg;
                            std::string line3;
                            while (std::getline(ss, line3, ',')) {

                                if (lastorfirst == false) {
                                    bs[courseCounter].LastName = line3;
                                    lastorfirst = true;
                                }
                                else {
                                    bs[courseCounter].FirstName = line3;
                                    lastorfirst = false;
                                };

                            };
                            notKey = false;

                        }

                        //check if REQ, REC, SUG, CHC, OER
                        if (keyCatcher(copy) == true) {//if it is
                            bs[courseCounter].key = *beg;
                        }

                        //cost finder
                        if (found != std::string::npos && costToggle == false) {//COSTS***
                            //outFile << ", , , , ,COST1" << *beg << std::endl;
                            bs[courseCounter].convCost = priceConverter(*beg);
                            bs[courseCounter].Cost = bs[courseCounter].convCost + bs[courseCounter].Cost;
                            costToggle = true;
                        }
                        else if (found != std::string::npos && costToggle == true) {
                            costToggle = false;
                        };
                        tokenNumber++;//this will increment the tracking int
                    };
                };
            };

        };

    };

    //Use this to map the position of the Nolo Courses from bookstore
    std::map<int, bookStruct>NOLOs;

    int courseNameCounter = 0;//to help set the data onto the map
    int mapCounter = 0;//used for for loops
    //Map the Nolos
    outFile << bs[courseNameCounter].Subject << std::endl;
    for (int a = 1; a < 900; a++) {
        if (bs[courseNameCounter].Cost < NoLoNum && keyCatcher(bs[courseNameCounter].key) == true) {//If NoLo
            NOLOs[mapCounter] = bs[courseNameCounter];//sets current position to map
            //make sure full
            if (NOLOs[mapCounter].Cost > 0) {
                outFile << NOLOs[mapCounter].CRN << ","
                    << NOLOs[mapCounter].Subject << ","
                    << NOLOs[mapCounter].Number << ","
                    << NOLOs[mapCounter].Section << ","
                    << NOLOs[mapCounter].CourseName << ","
                    << NOLOs[mapCounter].Cost << ","
                    << "NOLO" << ","//NOLO
                    << NOLOs[mapCounter].LastName << ","
                    << NOLOs[mapCounter].FirstName << ","
                    << NOLOs[mapCounter].Email << std::endl;
                };
            mapCounter++;//increment map
        };
        courseNameCounter++;
    };
    inFile.close();
    outFile.close();
    return bookstore;
};

//Catches overflow of rows
std::string Reader::regEndCatcher(std::string crust) {
    //erase each possile ending to the line
    //this error occurs due to there not being a comma at the end of a line in a .csv file
    std::string online = "ONLINE";
    std::string::size_type a = crust.find(online);
    if (a != std::string::npos)
        crust.erase(a, online.length());

    std::string stretr = "STRETR";
    std::string::size_type b = crust.find(stretr);
    if (b != std::string::npos)
        crust.erase(b, stretr.length());

    std::string gregg = "GREGG";
    std::string::size_type c = crust.find(gregg);
    if (c != std::string::npos)
        crust.erase(c, gregg.length());

    std::string nauto = "NAUTO";
    std::string::size_type d = crust.find(nauto);
    if (d != std::string::npos)
        crust.erase(d, nauto.length());

    std::string intern = "INTERN";
    std::string::size_type e = crust.find(intern);
    if (e != std::string::npos)
        crust.erase(e, intern.length());

    std::string nwell = "NWELL";
    std::string::size_type f = crust.find(nwell);
    if (f != std::string::npos)
        crust.erase(f, nwell.length());

    std::string offcmp = "OFFCMP";
    std::string::size_type g = crust.find(offcmp);
    if (g != std::string::npos)
        crust.erase(g, offcmp.length());

    std::string clincl = "CLINCL";
    std::string::size_type h = crust.find(clincl);
    if (h != std::string::npos)
        crust.erase(h, clincl.length());

    //return trimmed line
    return crust;
};

//check if REQ, REC, SUG, CHC, OER, BR
bool Reader::keyCatcher(std::string key) {
    if (key == "REQ") {//if REQ
        return true;
    }
    else if (key == "REC") {//if REC
        return true;
    }
    /*
    else if (key == "SUG") {//if SUG
        return true;
    }*/
    else if (key == "CHC") {//if CHC
        return true;
    }
    else if (key == "OER") {//if OER
        return true;
    }
    else if (key == "BR") {//if BR
        return true;
    }
    else {
        return false;
    }

};

//convert the string 
double Reader::priceConverter(std::string toConvert) {
    //convert into doubles
    //USE PRICE FROM UI

    //clear all spaces and dollar signs
    std::string::iterator end_pos = std::remove(toConvert.begin(), toConvert.end(), ' ');
    toConvert.erase(end_pos, toConvert.end());
    std::string::iterator end_money = std::remove(toConvert.begin(), toConvert.end(), '$');
    toConvert.erase(end_money, toConvert.end());
    //if characters are found return false
    for (int a = 0; a < toConvert.length(); a++) {
        char b = toConvert[a];
        if (isalpha(b)) {
            return false;
        };
    };
    if (toConvert.empty())return false;
    std::string::size_type sz;     // alias of size_t
    double Cost = std::stod(toConvert);
    return Cost;
};