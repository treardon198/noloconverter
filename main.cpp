#include <cstdlib>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

#include "MyForm.h"
using namespace NoLoConverter;//for Form usage
[STAThreadAttribute]//this helps the Form process openFileDialog


int main() {
    //open the Form
    
    //This will run the Form and then the input from the Form will decide the calibrations for the program
    MyForm mf;
    mf.ShowDialog();

    return 0;
};